package NakarmZwierzaki.Animal;

public class Fish extends Animal {

    public Fish(String name) {
        super(name);
    }

    @Override
    public void makeNoise() {
        System.out.println("blob blob");
    }

    @Override
    public void SayYourName() {
        System.out.println(String.format("I'm %s the Fish", this.getName()));


    }
}
