package NakarmZwierzaki.Animal;

import NakarmZwierzaki.Dishes.Dish;
import NakarmZwierzaki.Dishes.DishType;

public class Bird extends Animal {

    public Bird(String name) {
        super(name);
        this.addFavouriteDish(new Dish(DishType.GRAIN, 0.2));
    }

    @Override
    public void makeNoise() {
        System.out.println("ćwir ćwir");
    }

    @Override
    public void SayYourName() {
        System.out.println("I'm Birdy " + this.getName());
    }
}
