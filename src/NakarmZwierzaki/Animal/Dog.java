package NakarmZwierzaki.Animal;

import NakarmZwierzaki.Dishes.Dish;
import NakarmZwierzaki.Dishes.DishType;

import java.util.List;

public class Dog extends Animal {

    public Dog(String name) {
        super(name);
        this.addFavouriteDish(new Dish(DishType.BONES, 0.5));
    }

    @Override
    public void makeNoise() {
        System.out.println("Hau Hau!");
    }

    @Override
    public void SayYourName() {
        System.out.println(String.format("I'm %s the Dog", this.getName()));
    }


}
