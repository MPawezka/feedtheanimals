package NakarmZwierzaki.Animal;

import NakarmZwierzaki.Dishes.Dish;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public abstract class Animal {

    private String name;

    private List<Dish> favouriteDishes = new ArrayList<>();

    public abstract String makeNoise();
    public abstract String sayYourName();

    public Animal(String name) {
        this.name = name;
    }

    public Animal(String name, List<Dish> favouriteDishes) {
        this.name = name;
        this.favouriteDishes = favouriteDishes;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Dish> getFavouriteDishes() {
        return favouriteDishes;
    }

    public void setFavouriteDishes(List<Dish> favouriteDishes) {
        this.favouriteDishes = favouriteDishes;
    }

    public void addFavouriteDish(Dish dish) {
        if (!favouriteDishes.contains(dish)) {
            favouriteDishes.add(dish);
        }
    }

    public void feed(Dish dish){
        int dishIndex = favouriteDishes.indexOf(dish);
        double currentAmount = favouriteDishes.get(dishIndex).getAmount();
        if (dishIndex >= 0) {
            double newAmount = currentAmount - dish.getAmount() <=0 ? 0 : currentAmount - dish.getAmount();

            favouriteDishes.get(dishIndex).setAmount(newAmount);
        }
    }



    @Override
    public String toString() {
        return String.format("Zwierze [imie: %s, ulubione potrawy: %s]", name, favouriteDishes);
    }
}
