package NakarmZwierzaki.Animal;

import NakarmZwierzaki.Dishes.Dish;
import NakarmZwierzaki.Dishes.DishType;

public class Cat extends Animal {


    public Cat(String name) {
        super(name);
        this.addFavouriteDish(new Dish(DishType.MILK, 0.5));
    }

    @Override
    public void makeNoise() {
        System.out.println("Meeeeeow!");
    }

    @Override
    public void SayYourName() {
        System.out.println(String.format("I'm %s the Cat", this.getName()));
    }
}
