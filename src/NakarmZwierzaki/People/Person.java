package NakarmZwierzaki.People;

import NakarmZwierzaki.Animal.Animal;
import NakarmZwierzaki.Dishes.Dish;

import java.util.ArrayList;
import java.util.List;

public class Person {
    private String name;
    private List<Dish> availableDishes = new ArrayList<>();
    private List<Animal> myAnimals = new ArrayList<>();

    public Person() {
        this("Steve");
    }

    public Person(String name, List<Dish> availableDishes, List<Animal> myAnimals) {
        this.name = name;
        this.availableDishes = availableDishes;
        this.myAnimals = myAnimals;
    }

    public Person(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Dish> getAvailableDishes() {
        return availableDishes;
    }

    public void setAvailableDishes(List<Dish> availableDishes) {
        this.availableDishes = availableDishes;
    }

    public List<Animal> getMyAnimals() {
        return myAnimals;
    }

    public void setMyAnimals(List<Animal> myAnimals) {
        this.myAnimals = myAnimals;
    }

    public void addAnimal(Animal animal) {
        if (!myAnimals.contains(animal)) {
            myAnimals.add(animal);
        }
    }

    @Override
    public String toString() {
        return String.format("Person %s, my Animals: %s, my ", name, availableDishes, myAnimals);
    }
}
