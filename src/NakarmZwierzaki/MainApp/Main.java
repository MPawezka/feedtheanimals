package NakarmZwierzaki.MainApp;

import NakarmZwierzaki.Animal.Animal;
import NakarmZwierzaki.Animal.Bird;
import NakarmZwierzaki.Animal.Cat;
import NakarmZwierzaki.Animal.Dog;
import NakarmZwierzaki.Dishes.Dish;
import NakarmZwierzaki.Dishes.DishType;
import NakarmZwierzaki.People.Person;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        Person tomasz = new Person("Tomasz");

        Animal burek = new Dog("Burek");
        Animal siersciuch = new Cat("Sierściuch");
        Animal ptaszek = new Bird("Cwirek");

        burek.addFavouriteDish(new Dish(DishType.HAM, 1.0));


        tomasz.addAnimal(burek);
        tomasz.addAnimal(siersciuch);
        tomasz.addAnimal(ptaszek);

        System.out.println(tomasz);


    }
}
