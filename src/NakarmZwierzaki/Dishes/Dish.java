package NakarmZwierzaki.Dishes;

public class Dish {
   private DishType dishType;
   private double amount;


    public Dish() {
        this(DishType.UNDEFINED, 0.0);
    }

    public Dish(DishType dishType, double amount) {
        this.dishType = dishType;
        this.amount = amount;
    }

    public DishType getDishType() {
        return dishType;
    }

    public void setDishType(DishType dishType) {
        this.dishType = dishType;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }


    @Override
    public String toString() {
        return String.format("Dish [type: %s, amount: %.1f]", dishType, amount);
    }
}
