package NakarmZwierzaki.Dishes;

public enum DishType {
    HAM("Szynka"),
    MILK("Mleko"),
    FRUIT("Owoce"),
    BONES("Kości"),
    GRAIN("Ziarno"),
    UNDEFINED("Nieokreślone");

    private String name;

    DishType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return name;
    }
}
